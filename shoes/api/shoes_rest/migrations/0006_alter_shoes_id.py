# Generated by Django 4.0.3 on 2022-10-20 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0005_rename_closet_name_shoes_bin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoes',
            name='id',
            field=models.PositiveIntegerField(primary_key=True, serialize=False),
        ),
    ]
