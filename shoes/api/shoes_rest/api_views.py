from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .acls import get_photo

from .models import Shoes, BinVO
from common.json import ModelEncoder

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
      "import_href",
      "closet_name",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
      "manufacturer",
      "model",
      "color",
      "picture_url",
      "id",
    ]

    def get_extra_data(self, o):
        return {"closet_name": o.bin.closet_name}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
      "manufacturer",
      "model",
      "color",
      "picture_url",
      "bin",
    ]
    encoders = {
      "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = f"/api/bins/{bin_vo_id}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        photo = get_photo(content["manufacturer"], content["model"])
        content.update(photo)
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
          shoes,
          encoder=ShoesDetailEncoder,
          safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        Shoes.objects.filter(id=pk).update(**content)
        location = Shoes.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
