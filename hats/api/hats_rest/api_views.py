from django.shortcuts import render
from .models import Hats, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo

from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["style_name", "picture_url", "color", "fabric", "id"]

    def get_extra_data(self, o):
        return {"closet_name": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = f"/api/locations/{location_vo_id}/"
            locations = LocationVO.objects.get(import_href=location_href)
            content["location"] = locations
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"},
                status=400,
            )

        photo = get_photo(content["style_name"], content["color"])
        content.update(photo)
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, pk):

    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(id = content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Location does not exist"},
            status=400,
            )
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
