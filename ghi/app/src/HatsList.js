import React from "react";
import { Link } from 'react-router-dom';

class HatsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hats: []
    }
  };

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({hats: data.hats});
    }
  }

  handleDelete(id) {
    fetch(`http://localhost:8090/api/hats/${id}/`, { method: 'DELETE' })
    this.setState({hats: this.state.hats.filter(hat => hat.id !== id)});
  }

  render() {
    return (
      <>
        {/* <div className="row"> */}
        <div className="px-4 py-5 my-5 text-center">
          <div className="col-lg-6 mx-auto text-center">
            {/* <p className="lead mb-4">
            Your Virtual Hat Closet!
            </p> */}
            <h1 className="display-5 fw-bold">Your Virtual Hat Closet!</h1>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-dark btn-lg px-4 gap-3">Add a hat</Link>
            </div>
          </div>
        </div>
        <br></br>
        <div className="row">
        {this.state.hats.map(hat => {
            return (
              <div key={hat.href} className="col-md-4">
                <div className="card mb-2 shadow">
                  <img src={hat.picture_url} className="card-img-top" style={{height: 400}} />
                    <div className="card-body">
                      <h4 className="card-title">{hat.style_name} {hat.fabric}</h4>
                      <h6 className="card-subtitle mb-2 text-muted">{hat.color}</h6>
                      <p>Hats are hanging in {hat.closet_name}</p>
                      <button type="button" className="btn btn-dark" onClick={() => this.handleDelete(hat.id)}>Delete</button>
                    </div>
                  </div>
                </div>
            );
          })}
          </div>
      </>
    );
  };
}

export default HatsList;
