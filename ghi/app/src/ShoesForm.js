import React from 'react';


class ShoeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
      model: "",
      color: "",
      bin: "",
      bins: []
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
    this.handleChangeModel = this.handleChangeModel.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeBin = this.handleChangeBin.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    const binId = data.bin;
    delete data.bins;

    const shoesUrl = `http://localhost:8080/api/bins/${binId}/shoes/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoes = await response.json();
      console.log(newShoes);
      this.setState({
        manufacturer: '',
        model: '',
        color: '',
        bin: '',
      });
      window.location.replace('http://localhost:3000/shoes/')
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({ bins: data.bins });
    }
  }

  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleChangeModel(event) {
    const value = event.target.value;
    this.setState({ model: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangeBin(event) {
    const value = event.target.value;
    this.setState({ bin: value });
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacture" id="manufacture" className="form-control"  value={this.state.manufacturer}/>
                <label htmlFor="manufacture">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeModel} placeholder="Model" required type="text" name="model" id="model" className="form-control" value={this.state.model}/>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select" value={this.state.bin}>
                  <option value="">Choose a Closet</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              {/* <div className="mb-3">
                <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select" value={this.state.bin}>
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>{bin.bin_number}</option>
                    )
                  })}
                </select>
              </div> */}
              <button className="btn btn-dark">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


export default ShoeForm;
