import React from 'react';

class HatsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      style_name: "",
      fabric: "",
      color: "",
      location: "",
      locations: []
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeStyle_name = this.handleChangeStyle_name.bind(this);
    this.handleChangeFabric = this.handleChangeFabric.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    const locationId = data.location;
    delete data.locations;

    const hatsUrl = `http://localhost:8090/api/locations/${locationId}/hats/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatsUrl, fetchConfig);
    if (response.ok) {
      const newHats = await response.json();
      console.log(newHats);
      this.setState({
        style_name: '',
        fabric: '',
        color: '',
        location: '',
      });
      window.location.replace('http://localhost:3000/hats/')
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({ locations: data.locations });
    }
  }

  handleChangeStyle_name(event) {
    const value = event.target.value;
    this.setState({ style_name: value });
  }

  handleChangeFabric(event) {
    const value = event.target.value;
    this.setState({ fabric: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangeLocation(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeStyle_name} placeholder="Style_name" required type="text" name="style_name" id="style_name" className="form-control"  value={this.state.style_name}/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={this.state.fabric}/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeLocation} required name="location" id="location" className="form-select" value={this.state.location}>
                  <option value="">Choose a Closet</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-dark">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


export default HatsForm;
