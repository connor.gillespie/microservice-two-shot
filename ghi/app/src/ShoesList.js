import React from "react";
import { Link } from 'react-router-dom';


class ShoesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      shoes: []
    }

    this.handleDelete = this.handleDelete.bind(this);
  };

  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({shoes: data.shoes});
    }
  }

  handleDelete(id) {
    fetch(`http://localhost:8080/api/shoes/${id}/`, { method: 'DELETE' })
    this.setState({shoes: this.state.shoes.filter(shoe => shoe.id !== id)});
  }



  render() {
    return (
      <>
        {/* <div className="row"> */}
        <div className="px-4 py-5 my-5 text-center">
          <div className="col-lg-6 mx-auto text-center">
            {/* <p className="lead mb-4">
            Your Virtual Shoe Bin!
            </p> */}
            <h1 className="display-5 fw-bold">Your Virtual Shoe Bin!</h1>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/shoes/new" className="btn btn-dark btn-lg px-4 gap-3">Add a shoe</Link>
            </div>
          </div>
        </div>
        <br></br>
        <div className="row">
        {this.state.shoes.map(shoe => {
            return (
              <div key={shoe.href} className="col-md-4">
                <div className="card mb-2 shadow">
                  <img src={shoe.picture_url} className="card-img-top" style={{height: 400}} />
                    <div className="card-body">
                      <h4 className="card-title">{shoe.manufacturer} {shoe.model}</h4>
                      <h6 className="card-subtitle mb-2 text-muted">{shoe.color}</h6>
                      <p>Shoes are lying in {shoe.closet_name}</p>
                      <button type="button" className="btn btn-dark" onClick={() => this.handleDelete(shoe.id)}>Delete</button>
                    </div>
                  </div>
                </div>
            );
          })}
          </div>
      </>
    );
  };
}

export default ShoesList;
