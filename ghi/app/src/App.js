import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import MainPage from './MainPage';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList />} />
              <Route path="new" element={<ShoesForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsList />} />
              <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
