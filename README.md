# Wardrobify

Team:

* Marvin Lee - Shoes
* Connor Gillespie - Hats

## Design

## Shoes microservice

Basic model for shoes with properties - manufacturer, model, color, picture_url and bin.
BinVO model with properties - import_href and closet_name.
Poller application implemented to pull Bin data from Wardrobe API.
React components to show list of shoes with their details and a form to create a new shoe.



## Hats microservice

Install hats-rest into Installed Apps to install the Django app. Make a simple Hats model adding in as many of the fields we need to track as possible. Make a function view to list the Hats model. Create and configure URLs file for Hats app, then include URLs from app into hats_project.
